# -*- coding: utf-8 -*-
#
# Na cidade de Sasha Grey chegou o parte de diversões. Chegou o palhaço louco.
# Uma das atrações é a roda gigante.
# Na roda gigante só podem entrar pessoar com dois sobrenomes, que tenham mais de 1,61m de altura e que não estejam sozinhas.
# Porém, se a pessoa tiver exatamente 1,50 e trouxer 4 acompanhantes, pode entrar. Além disso, as cabines são numeradas, e se
# a pessoa entrar na cabine de número 10 e for a centésima pessoa a fazer isso ganha o prêmio do palhaço louco.
# Forenecido o contexto acima, elabore um algoritmo na abordagem topdown que indique se uma pessoa pode entrar no brinquedo e
# se ela será premiada ou não.
#
# -------- Pacotes ---------
import string
import random


# PRIMEIRA FUNÇÃO: Verifica se a pessoa tem pelo menos dois sobrenomes.


def nome():
  print('\n BEM VINDO ao Parque de Diversões da cidade de Sasha Grey!\nQuer andar na roda gigante? Vamos ver se você pode.')
  nome = input("\n\nQual o seu nome completo?")
  sobrenomes = sum([i.strip(string.punctuation).isalpha() for i in nome.split()])
  if int(sobrenomes) >= 3:
  	altura()
  if int(sobrenomes) < 3:
   print('\nSINTO MUITO! Você precisar ter pelo menos DOIS sobrenomes para entrar na roda gigante.')


#FUNÇÃO DE ALTURA: Verifica a altura do visitante.
 #Se ele tiver mais de 1,61m de altura, procede para a função acompanhado().
 #Se a pessoa tiver exatamente 1,50m, procede para a função tem150().
 #Se a altura for menor que 1,61m e diferente de 1,50m, segue para a função alturafail().

def altura():
 altura = int(input("\nQual a sua altura(em centímetros)?"))
 if altura >= 161:
  acompanhado()
 if altura == 150:
  tem150()
 if altura < 161 and altura != 150:
     alturafail()

# FUNÇÃO DE SOLIDÃO: Verifica se o visitante com mais de 1,61m de altura está sozinho. Se ele estiver, não pode entrar.
 # Se ele NÃO estiver sozinho, segue para a função premio().

def acompanhado():
 sozinho = input("\nVocê está sozinho(S/N)?")
 if sozinho == 's' or 'S':
  print('\nVocê precisa de amigos! Não é permitido entrar sozinho na roda gigante.')
 if sozinho == 'n' or 'N':
  premio()

#FUNÇÃO PARA VISITANTES COM 1,50M DE ALTURA: Caso o visitante tenha exatamente 1,50m de altura,
 # essa função verifica se ele trouxe consigo pelo menos 4 acompanhantes. Se sim, segue para a função
 # premio(). Se não, segue para alturafail().
def tem150():
 amigos = int(input("\nQuantos acompanhantes estão com você?"))
 if amigos != 4:
  	alturafail()
 if amigos == 4:
    premio()

#FUNÇÃO FINAL: Se o visitante preenche todos os requisitos necessários para entrar, essa função
# informa o visitante de que ele pode entrar. Se ele entrar na cabine de número 10 (1/10 de chance, considerando 10 cabines)
# e for a centésimo a fazer isso (1/100 de chance a cada cem passeios) ele será premiado (1/10 * 1/100 = 1/1000 de chance de
# estar nessa situação). O visitante premiado terá a chance de descabelar o palhaço louco!
#
# OBS: foi considerado que existem 10 cabines na roda-gigante e que a cada 100 passeios a contagem de visitantes na cabine
# é reiniciada. Com isso, um em cada 1000 visitantes será o centésimo da décima cabine, portanto, o premiado.
def premio():
    print("Pode entrar na roda gigante!")

    cadeira = random.randint(1,1000)
    if cadeira == 1000:
    	print ("\n   PARABÉNS! Você ganhou o prêmio.\n   Pode descabelar o palhaço louco!! \n")

#FUNÇÃO FAIL: Essa função informa o visitante de que ele não preenche os requisitos relativos a altura.
def alturafail():
	print('\nSINTO MUITO! Você precisar ter pelo menos 1,61m,\n OU\nter 1,50m e trazer 4 acompanhantes.')

nome()

 # 	etapa3()
 # input('Quantos acompanhantes vieram com você?') = am
