lugares = ["taiwan","montevideo","tokyo","zurich","amsterdam"]
def original():
 print("\n Lista original: ",lugares)
def inverter():
 lugares.reverse()
def organizar():
    lugares.sort()
def organizarinverso():
    lugares.sort(reverse=True)
original()

print("\n Lista em ordem alfabética: ",sorted(lugares))

original()

print("\n Lista em ordem alfabética reversa: ",sorted(lugares, reverse=True))

original()

print("\n Invertendo a lista...")

inverter()
original()

print("\n Revertendo a lista...")

inverter()
original()

print("\n Armazenando a lista em ordem alfabética...")

organizar()
original()

print("\n Armazenando a lista em ordem alfabética inversa...")

organizarinverso()
original()
