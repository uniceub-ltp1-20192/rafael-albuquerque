cogumelos = ["shitake","champignon","cubensis","shimeji","amanita"]
def original():
 print("\n Lista original: ", cogumelos)

def getsize():
 print("\n Tamanho da lista: ", len(cogumelos))

def inverter():
 cogumelos.reverse()

def organizar():
    cogumelos.sort()
    print("\n Armazenando a lista em ordem alfabética...")

def organizarinverso():
    print("\n Armazenando a lista em ordem alfabética inversa...")
    cogumelos.sort(reverse=True)

original()
print("\n Imprimindo a lista em ordem alfabética: ",sorted(cogumelos))
original()
print("\n Imprimindo a lista em ordem alfabética reversa: ",sorted(cogumelos, reverse=True))
original()
print("\n Invertendo a lista...")
inverter()
original()
print("\n Revertendo a lista...")
inverter()
original()
organizar()
original()
organizarinverso()
original()
getsize()
