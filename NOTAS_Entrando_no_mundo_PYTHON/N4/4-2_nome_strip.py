nome = "\tJohn\nSilva\t"
print("Normal:\n"+nome)
print("\nCom 'rstrip()':\n\n"+"'"+nome.rstrip()+"'")
print("\nCom 'lstrip()':\n\n"+"'"+nome.lstrip()+"'")
print("\nCom 'strip()':\n\n"+"'"+nome.strip()+"'")
