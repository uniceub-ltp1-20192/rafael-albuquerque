lugares = ["taiwan","montevideo","tokyo","zurich","amsterdam"]

print("Os três primeiros lugares:")
primeiros = lugares[:3]
for lugar in primeiros:
 print(lugar.title())

print("\n Os três ultimos lugares:")
ultimos = lugares[2:]
for lugar in ultimos:
 print(lugar.title())

print("\n Os três lugares do meio:")
meio = []
for i in range(1,4):
 meio.append(lugares[i])
for lugar in meio:
 print(lugar.title())
