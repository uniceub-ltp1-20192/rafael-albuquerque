convidados = ["Gandhi","Alan Watts","Lao Tse"]
def convite():
  for i in convidados:
    m = ", gostaria de comparecer ao meu jantar na sexta-feira?\n"
    print("\n"+i+m)

convite()

x = 'Gandhi'
print("ATENÇÃO: "+x+" não poderá comparecer.\n")
convidados[0] = "Raul seixas"

convite()

print("Caros amigos, encontrei uma mesa maior.")
convidados.insert(0, "Stan Lee")
convidados.insert(2, "Walt Disney")
convidados.append("Jim Henson")

convite()

print("Caros amigos, infelizmente terei que reduzir o número de convidados.")
while len(convidados)>2:
    print("\n"+convidados[0]+", infelizmente não poderei mais te convidar para o jantar de sexta-feira.")
    convidados.pop(0)
for i in convidados:
    m = ", você ainda está convidado para o jantar de sexta-feira!\n"
    print("\n"+i+m)
del convidados[:]
print(convidados)
