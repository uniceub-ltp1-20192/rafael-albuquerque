cebolas = 420 # Número de cebolas
cebolas_na_caixa = 260 # Número de cebolas encaixotadas
espaco_caixa = 30 # capacidade cebolística de cada caixa
caixas = 889 # Número de caixas em nosso poder
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa # Subtraindo o número total de cebolas pelo número de cebolas encaixotadas, obtendo o número de cebolas rebeldes
caixas_vazias = int(caixas - (cebolas_na_caixa/espaco_caixa)) # Calculando o número de caixas vazias
caixas_necessarias = int(cebolas_fora_da_caixa / espaco_caixa) # Número de caixas necessárias para encaixotar as cebolas sem caixas
print("Existem", cebolas_na_caixa, "cebolas encaixotadas") # Imprimindo o número de cebolas já encaixotadas
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa") # imprimindo o número de cebolas fora da caixa
print("Em cada caixa cabem", espaco_caixa, "cebolas") # Imprimindo a capacidade cebolística de cada caixa
print("Ainda temos,", caixas_vazias, "caixas vazias") # Imprimindo número de caixas sem cebola
print("Então, precisamos de", caixas_necessarias,"caixas para empacotar todas as cebolas") # Imprimindo o número de caixas necessárias para encaixotar as cebolas rebeldes
