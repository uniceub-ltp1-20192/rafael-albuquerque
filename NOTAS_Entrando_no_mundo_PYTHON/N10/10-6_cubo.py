lista = []


def raizcubica(n) :
    return (int)( n ** (1. / 3))

# An efficient solution to print
# perfect cubes between a and b
def makeLista(x,y) :

    rc1 = raizcubica(x)
    rc2 = raizcubica(y)

    for i in range(rc1, rc2 + 1):
            lista.append(i ** 3)

def exibir(list):
    for i in list:
        print(i)


makeLista(1, 100)
exibir(lista)
