def principal():
    global sem, idade, atr
    idade = int(input("Qual a sua idade? "))
    sem = int(input("Em qual semestre você está? "))
    atr = int(input("Quantos semestres você atrasou? "))
    quantofalta()

def quantofalta():
    totalsem = 8 + atr
    totalanos = totalsem/2
    falta = 8 - sem + atr
    anos = falta/2
    idadefinal = round(idade + anos)
    print("\nQuando você se formar, vai ter passado ", totalanos, "anos na faculdade.\nVocê vai se formar com ", idadefinal, " anos de idade, daqui a ", anos, "anos.")

principal()
