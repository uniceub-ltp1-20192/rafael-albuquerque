import menu
import dados
import re

def valid_nome(nome):
     if not re.match("^[A-Za-z]*$", nome):
       print ("\nAPENAS LETRAS SÃO ACEITAS. TENTE NOVAMENTE\n")
       return False
     else:
         return True

def valid_email(email):
    if not re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
      print ("\nEMAIL INVÁLIDO. TENTE NOVAMENTE")
      return False
    else:
        return True 
