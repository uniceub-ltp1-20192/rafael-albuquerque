import json
from random import randint
import menu
import validador
import re


global nome
# mydic = {}

# =============================== MÉTODO DE INSERÇÃO DE DADOS
def novo():
  with open("dados.json") as dados:
     mydic = json.load(dados)
     chave = newkey()
     print ("\nINSERIR NOVO REGISTRO")
     nome = input("\n NOME: ")
     if validador.valid_nome(nome) == False:
         novo()
     else:
         email = input("\n EMAIL: ")
         if validador.valid_email(email) == False:
           novo()
         else:
             mydic["usuarios"][chave] = {'ID': chave,'nome': nome,'email': email}

             with open("dados.json", "w") as dados:
              json.dump(mydic, dados, indent=4, sort_keys=True)
             menu.menu()


# =============================== GERADOR DE CHAVES
def newkey():
  chave = randint(0, 1000000000)
  with open("dados.json") as dados:
   mydic = json.load(dados)
  if chave not in mydic['usuarios']:
    return str(chave)
  else:
    newkey()


# =============================== MÉTODO DE CONSULTA
def buscar():
    with open('dados.json') as dados:
      mydic = json.load(dados)
      opcao = input("\n  1-Busca por ID\n  2-Busca por nome\n  3-Mostrar tudo")

      if opcao == "1":
          print(' Digite a ID que deseja buscar:')
          busca = input()
          if busca in mydic["usuarios"]:
             print(json.dumps(mydic['usuarios'][busca], indent=4, sort_keys=True))
             print("\n============= FIM DOS RESULTADOS ===========\n")
             print("\n Deseja fazer outra busca?\n   1-Sim\n   2-Não\n")
             opt = input()
             if opt == "1":
                 buscar()
             else:
                 menu.menu()
          else:

              print("\nNenhum resultado encontrado\n\n Deseja fazer outra busca?\n   1-Sim\n   2-Não\n")
              opt = input()
              if opt == "1":
                  buscar()
              else:
                  menu.menu()

      if opcao == "2":
         busca = input('Digite o nome que deseja buscar:')
         for i in mydic["usuarios"]:
          if busca in i["nome"] :
            print(i)
            print("\n============= FIM DOS RESULTADOS ===========\n")
            print("\n Deseja fazer outra busca?\n   1-Sim\n   2-Não\n")
            opt = input()
            if opt == "1":
                buscar()
            else:
                menu.menu()

          else:
            print("\nNenhum resultado encontrado\n\n Deseja fazer outra busca?\n   1-Sim\n   2-Não\n")
            opt = input()
            if opt == "1":
                buscar()
            else:
                menu.menu()

      if opcao == "3":
          print(json.dumps(dados, indent=4, sort_keys=True))
          print("============= FIM DOS RESULTADOS ===========")
          menu.menu()

# =============================== MÉTODO DE ALTERAÇÃO
def alterar():
   with open('dados.json') as dados:
      mydic = json.load(dados)
      elemento = input(' Digite a ID do registro que deseja alterar:')
      if elemento in mydic["usuarios"]:
         print(json.dumps(mydic['usuarios'][elemento], indent=4, sort_keys=True))
         opt = input("\nVocê deseja alterar Nome ou Email?\n   1-Nome\n   2-Email\n")
         if opt == "1":
             novonome = input("\n  NOVO NOME: ")
             mydic["usuarios"][elemento]["nome"] = novonome
             with open("dados.json", "w") as dados:
               json.dump(mydic, dados, indent=4, sort_keys=True)
             print("Alteração concluída com sucesso.")
             print(json.dumps(mydic['usuarios'][elemento], indent=4, sort_keys=True))
             print("\nDeseja fazer outra alteração?\n   1-Sim\n   2-Não\n")
             opt = input()
             if opt == "1":
                 alterar()
             else:
                 menu.menu()

         if opt == "2":
             novoemail = input("\n  NOVO EMAIL: ")
             mydic["usuarios"][elemento]["email"] = novoemail
             with open("dados.json", "w") as dados:
               json.dump(mydic, dados, indent=4, sort_keys=True)
             print("\nAlteração concluída com sucesso.")  
             print(json.dumps(mydic['usuarios'][elemento], indent=4, sort_keys=True))
             print("\nDeseja fazer outra alteração?\n   1-Sim\n   2-Não\n")
             opt = input()
             if opt == "1":
                 alterar()
             else:
                 menu.menu()




# =============================== MÉTODO PARA MOSTRAR TODO OS VALORES
def showdic():
    with open('dados.json') as dados:
     data = json.load(dados)
     print(json.dumps(data, indent=4, sort_keys=True))
