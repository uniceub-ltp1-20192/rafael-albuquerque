# Listas para armazenar e manipular os valores inseridos pelo usuário

notas = []
pesos = []
ncp   = [] # notas com peso

# Main function 
def principal():
  print("\nInsira uma nota, e em seguida seu respectivo peso.\n\nQuando tiver terminado, digite x para obter o resultado.\n")
  asknota()

# Defining a function to get input
def asknota():
  
  global n,p
  try:	
     n = int(input('Digite a nota: '))
     p = int(input('Digite o peso: '))
     register()
  except ValueError:           # Digitando uma letra ao invés de um número interrompe o registro de novos inputs
  	  cp()                    # e executa a função scp() e calc().
  	  calc()

# Lendo inputs e contando o número de notas inseridas pelo usuário
def register():
  global nm
  nm = 0
  notas.append(n)
  pesos.append(p)
  nm =+1
  asknota() # Volta para pedir um novo input


#   Com peso (cp): Multiplicando notas pelos seus respectivos pesos
def cp():
  for i in range(0, len(notas)):
   ncp.append(notas[i]*pesos[i])


# Calculando o resultado final
def calc():
  tcp = sum(ncp)/len(notas)    #  Média aritimética comum
  tsp = sum(notas)/len(notas)  #  Média ponderada


# Resultado final:
  print("\nMédia sem peso: ", tsp)
  print("Média com peso: ", tcp, "\n")

principal()